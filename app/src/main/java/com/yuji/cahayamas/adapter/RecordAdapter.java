package com.yuji.cahayamas.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.yuji.cahayamas.EditActivity;
import com.yuji.cahayamas.R;
import com.yuji.cahayamas.models.Record;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.ViewHolder> {

    private ArrayList<Record> records;
    private Activity activity;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final Chip cpDate;
        private final TextView txAmount;
        private final TextView txNote;
        private final TextView txCategory;
        private final FloatingActionButton fabEdit;

        public ViewHolder(View view) {
            super(view);
            cpDate = (Chip) view.findViewById(R.id.date);
            txAmount = (TextView) view.findViewById(R.id.amount);
            txCategory = (TextView) view.findViewById(R.id.category);
            txNote = (TextView) view.findViewById(R.id.note);
            fabEdit = (FloatingActionButton) view.findViewById(R.id.edit);
        }

        public Chip getCpDate() {
            return cpDate;
        }

        public TextView getTxAmount() {
            return txAmount;
        }

        public TextView getTxNote() {
            return txNote;
        }

        public TextView getTxCategory() {
            return txCategory;
        }

        public FloatingActionButton getFabEdit() {
            return fabEdit;
        }
    }

    public RecordAdapter(Activity activity, ArrayList<Record> records) {
        this.activity = activity;
        this.records = records;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_record_list, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        Record current = records.get(position);

        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        String amount = String.format("Rp. %d", current.getAmount());
        String category = current.getCategory();
        String note = current.getNote();
        String dateStr = "";

        try {
            Date date = sf.parse(current.getDate());
            sf = new SimpleDateFormat("E, MMM dd yyyy");
            dateStr = sf.format(date);
        } catch (ParseException ignored) {

        }
        viewHolder.getTxAmount().setText(amount);
        viewHolder.getTxCategory().setText(category);
        viewHolder.getTxNote().setText(note);
        viewHolder.getCpDate().setText(dateStr);

        if (position > 1) {
            Record before = records.get(position - 0);
            boolean dateInvisible = current.getDate().equals(before.getDate());
            if (dateInvisible)
                viewHolder.getCpDate().setVisibility(View.GONE);
            else
                viewHolder.getCpDate().setVisibility(View.VISIBLE);
        }

        viewHolder.getFabEdit().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit(position, current);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return records.size();
    }

    protected void edit(int position, Record record) {
        Intent intent = new Intent(activity, EditActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("id", record.getId());
        intent.putExtra("type", record.getType());
        intent.putExtra("category", record.getCategory());
        intent.putExtra("date", record.getDate());
        intent.putExtra("amount", record.getAmount());
        intent.putExtra("note", record.getNote());
        activity.startActivityForResult(intent, 2);
    }
}
