package com.yuji.cahayamas;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.yuji.cahayamas.adapter.TabAdapter;
import com.yuji.cahayamas.models.DaoSession;
import com.yuji.cahayamas.models.Record;
import com.yuji.cahayamas.models.RecordDao;

import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity {
    LinearLayout lnHeader;
    FragmentTransaction ft;
    BalanceFragment frBalance;
    ChartFragment frChart;
    RecordFragment frIncome;
    RecordFragment frOutcome;
    FloatingActionButton fabAdd;
    TabLayout tlRecord;
    RecordDao recordDao;
    ViewPager viewPager;
    boolean headerToggled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        recordDao = daoSession.getRecordDao();

        setupAnimation();
        setupViews();
    }

    protected void setupViews() {
        lnHeader = findViewById(R.id.header);
        fabAdd = findViewById(R.id.add);
        tlRecord = findViewById(R.id.list_tab);
        viewPager = findViewById(R.id.pager);
        ft = getSupportFragmentManager().beginTransaction();
        frBalance = new BalanceFragment();
        frChart = new ChartFragment();
        frIncome = new RecordFragment();
        frOutcome = new RecordFragment();

        ft.replace(R.id.header, frBalance);
        ft.add(R.id.header, frChart);
        ft.commit();

        updateHeader();
        setupTab();

        lnHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerToggled = !headerToggled;
                updateHeader();
            }
        });

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });

    }

    protected void setupAnimation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.coordinator)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }
    }

    protected void updateHeader() {
        ft = getSupportFragmentManager().beginTransaction();

        if (headerToggled) {
            ft.show(frChart);
        } else {
            ft.hide(frChart);
        }
        ft.commit();
    }

    protected void setupTab() {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        ArrayList<Record> records1 = new ArrayList<>(recordDao.queryBuilder().where(RecordDao.Properties.Type.eq("Pemasukan")).orderDesc(RecordDao.Properties.Date).list());
        ArrayList<Record> records2 = new ArrayList<>(recordDao.queryBuilder().where(RecordDao.Properties.Type.eq("Pengeluaran")).orderDesc(RecordDao.Properties.Date).list());

        frIncome.setRecords(records1);
        frOutcome.setRecords(records2);
        ;

        adapter.addFragment(frIncome, "Pemasukan");
        adapter.addFragment(frOutcome, "Pengeluaran");
        viewPager.setAdapter(adapter);
        tlRecord.setupWithViewPager(viewPager);

    }

    protected void add() {
        Intent intent = new Intent(getApplicationContext(), AddActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (1):
            case (2): {
                if (resultCode == 1) {
                    int position = data.getIntExtra("position", 0);

                    long id = data.getLongExtra("id", 0);
                    long amount = data.getLongExtra("amount", 0);
                    String type = data.getStringExtra("type");
                    String category = data.getStringExtra("category");
                    String date = data.getStringExtra("date");
                    String note = data.getStringExtra("note");

                    Record record = new Record(id, type, category, note, date, amount);
                    RecordFragment current = type.equals("Pemasukan") ? frIncome : frOutcome;

                    if (requestCode == 1)
                        current.addRecord(record);
                    else
                        current.updateRecord(record, position);
                    frBalance.updateBalance();
                }
                break;
            }
        }
    }
}