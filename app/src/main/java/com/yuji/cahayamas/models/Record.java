package com.yuji.cahayamas.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;

@Entity(indexes = {
        @Index(value = "id ASC", unique = true)
})

public class Record {
    @Id
    private Long id;

    @NotNull
    private String type;
    @NotNull
    private String category;
    private String note;
    @NotNull
    private String date;
    @NotNull
    private Long amount;

@Generated(hash = 2044169610)
public Record(Long id, @NotNull String type, @NotNull String category,
        String note, @NotNull String date, @NotNull Long amount) {
    this.id = id;
    this.type = type;
    this.category = category;
    this.note = note;
    this.date = date;
    this.amount = amount;
}
@Generated(hash = 477726293)
public Record() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getType() {
    return this.type;
}
public void setType(String type) {
    this.type = type;
}
public String getCategory() {
    return this.category;
}
public void setCategory(String category) {
    this.category = category;
}
public String getNote() {
    return this.note;
}
public void setNote(String note) {
    this.note = note;
}
public String getDate() {
    return this.date;
}
public void setDate(String date) {
    this.date = date;
}
public Long getAmount() {
    return this.amount;
}
public void setAmount(Long amount) {
    this.amount = amount;
}
}
