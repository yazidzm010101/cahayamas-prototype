package com.yuji.cahayamas.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity (indexes = {
        @Index(value = "username ASC", unique = true)
})

public class User {
    @Id
    private Long id;

    @NotNull
    private String name;
    @NotNull
    private String username;
    @NotNull
    private String password;
@Generated(hash = 981351910)
public User(Long id, @NotNull String name, @NotNull String username,
        @NotNull String password) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.password = password;
}
@Generated(hash = 586692638)
public User() {
}
public Long getId() {
    return this.id;
}
public void setId(Long id) {
    this.id = id;
}
public String getUsername() {
    return this.username;
}
public void setUsername(String username) {
    this.username = username;
}
public String getPassword() {
    return this.password;
}
public void setPassword(String password) {
    this.password = password;
}
public String getName() {
    return this.name;
}
public void setName(String name) {
    this.name = name;
}
}
