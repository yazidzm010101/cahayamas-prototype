package com.yuji.cahayamas;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.yuji.cahayamas.models.DaoSession;
import com.yuji.cahayamas.models.User;
import com.yuji.cahayamas.models.UserDao;

public class LoginActivity extends AppCompatActivity {
    private TextInputLayout txUsername;
    private TextInputLayout txPassword;
    private Button btLogin;
    private Button btRegister;
    private UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();

        setupViews();
    }

    protected void setupViews() {
        txUsername = findViewById(R.id.username);
        txPassword = findViewById(R.id.password);
        btLogin = findViewById(R.id.login);
        btRegister = findViewById(R.id.register);

        checkAllValid();

        txUsername.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllValid();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllValid();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegister();
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


    }

    protected void startRegister() {
        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    protected void checkAllValid() {
        String username = txUsername.getEditText().getText().toString();
        String password = txPassword.getEditText().getText().toString();
        boolean isValid = !username.isEmpty() && !password.isEmpty();

        btLogin.setEnabled(isValid);
    }

    protected void login() {
        String username = txUsername.getEditText().getText().toString();
        String password = txPassword.getEditText().getText().toString();

        User user = userDao.queryBuilder().where(
                UserDao.Properties.Username.eq(username),
                UserDao.Properties.Password.eq(password)
        ).unique();

        if (user == null) {
            Toast.makeText(getApplicationContext(), "Invalid credentials", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(getApplicationContext(), "Login success", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

}