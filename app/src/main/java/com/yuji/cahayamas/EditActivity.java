package com.yuji.cahayamas;

import android.content.Intent;
import android.widget.Toast;

import com.yuji.cahayamas.models.Record;
import com.yuji.cahayamas.models.RecordDao;

public class EditActivity extends AddActivity {

    @Override
    public void setupViews() {
        super.setupViews();

        String type = getIntent().getStringExtra("type");
        String category = getIntent().getStringExtra("category");
        String date = getIntent().getStringExtra("date");
        String note = getIntent().getStringExtra("note");
        long amount = getIntent().getLongExtra("amount", 0);

        atType.setText(type);
        atCategory.setText(category);
        txDate.getEditText().setText(date);
        txNote.getEditText().setText(note);
        txAmount.getEditText().setText(String.format("%d", amount));
        btAdd.setText("Save");
    }

    @Override
    public void add() {
        int position = getIntent().getIntExtra("position", 0);
        long id = getIntent().getLongExtra("id", 0);
        String type = atType.getText().toString();
        String category = atCategory.getText().toString();
        String date = txDate.getEditText().getText().toString();
        String note = txNote.getEditText().getText().toString();
        long amount = Long.parseLong(txAmount.getEditText().getText().toString());

        Intent resultIntent = new Intent();

        Record record = recordDao.queryBuilder().where(RecordDao.Properties.Id.eq(id)).unique();
        if (record == null) {
            Toast.makeText(getApplicationContext(), "Failed to save changes of the data", Toast.LENGTH_LONG).show();
            return;
        }
        record.setType(type);
        record.setCategory(category);
        record.setDate(date);
        record.setNote(note);
        record.setAmount(amount);
        recordDao.update(record);
        Toast.makeText(getApplicationContext(), "Data successfully saved", Toast.LENGTH_LONG).show();


        resultIntent.putExtra("position", position);
        resultIntent.putExtra("id", id);
        resultIntent.putExtra("type", type);
        resultIntent.putExtra("category", category);
        resultIntent.putExtra("date", date);
        resultIntent.putExtra("note", note);
        resultIntent.putExtra("amount", amount);
        setResult(1, resultIntent);
        finish();
    }
}
