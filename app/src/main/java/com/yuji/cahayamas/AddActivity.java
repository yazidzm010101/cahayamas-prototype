package com.yuji.cahayamas;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.yuji.cahayamas.models.DaoSession;
import com.yuji.cahayamas.models.Record;
import com.yuji.cahayamas.models.RecordDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddActivity extends AppCompatActivity {
    String[] types = {"Pengeluaran", "Pemasukan"};
    String[] category1 = {"Makanan dan Bahan Dapur", "Transportasi", "Perabotan", "Lain-lain"};
    String[] category2 = {"Pulsa", "Perhiasan", "Lain-lain"};

    MaterialAutoCompleteTextView atType;
    MaterialAutoCompleteTextView atCategory;
    TextInputLayout txDate;
    TextInputLayout txAmount;
    TextInputLayout txNote;
    Button btAdd;
    RecordDao recordDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        recordDao = daoSession.getRecordDao();

        setupViews();
    }

    protected void setupViews() {
        atType = findViewById(R.id.type);
        atCategory = findViewById(R.id.category);
        txDate = findViewById(R.id.date);
        txAmount = findViewById(R.id.amount);
        txNote = findViewById(R.id.note);
        btAdd = findViewById(R.id.add);

        setupType();
        setupCategory();
        checkAllValid();

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });

        txDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDate();
            }
        });

        atType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setupCategory();
                checkAllValid();
            }
        });

        atCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkAllValid();
            }
        });

        txAmount.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllValid();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txNote.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkAllValid();
            }
        });
    }

    protected void setupType() {
        ArrayAdapter adapter;
        adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, types);
        atType.setAdapter(adapter);
    }

    protected void setupCategory() {
        ArrayAdapter adapter;
        String type = atType.getText().toString();
        if (type.equals("Pengeluaran")) {
            adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, category1);
            atCategory.setText(category1[0]);
        } else {
            adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, category2);
            atCategory.setText(category2[0]);
        }
        atCategory.setAdapter(adapter);
    }

    protected void showDate() {
        int[] dates = stringDatify(txDate.getEditText().getText().toString());

        DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0,
                                  int arg1, int arg2, int arg3) {
                txDate.getEditText().setText(dateStringify(arg3, arg2 + 1, arg1));
                checkAllValid();
            }
        };

        DatePickerDialog dpDate = new DatePickerDialog(AddActivity.this,
                datePickerListener, dates[2], dates[1], dates[0 ]);

        dpDate.show();
    }

    protected String dateStringify(int day, int month, int year) {
        return String.format("%02d/%02d/%04d", day, month, year);
    }

    protected int[] stringDatify(String strDate) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");

        try {
            calendar.setTime(sf.parse(strDate));
        } catch (ParseException ignored) {}

        return new int[]{
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.YEAR)
        };
    }

    protected void checkAllValid() {
        String type = atType.getText().toString();
        String category = atCategory.getText().toString();
        String amount = txAmount.getEditText().getText().toString();
        String date = txDate.getEditText().getText().toString();
        String note = txNote.getEditText().getText().toString();
        boolean isValid = !type.isEmpty() && !category.isEmpty() && !amount.isEmpty() && !date.isEmpty() && !note.isEmpty();
        btAdd.setEnabled(isValid);
    }

    protected void add() {
        String type = atType.getText().toString();
        String category = atCategory.getText().toString();
        String date = txDate.getEditText().getText().toString();
        String note = txNote.getEditText().getText().toString();
        long amount = Long.parseLong(txAmount.getEditText().getText().toString());

        Intent resultIntent = new Intent();

        Record record = new Record(null, type, category, note, date, amount);
        long id = recordDao.insert(record);

        if (id < 1) {
            Toast.makeText(getApplicationContext(), "Failed to add new data", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(getApplicationContext(), "Successed to add new data", Toast.LENGTH_LONG).show();


        resultIntent.putExtra("id", id);
        resultIntent.putExtra("type", type);
        resultIntent.putExtra("category", category);
        resultIntent.putExtra("date", date);
        resultIntent.putExtra("note", note);
        resultIntent.putExtra("amount", amount);
        setResult(1, resultIntent);
        finish();
    }
}