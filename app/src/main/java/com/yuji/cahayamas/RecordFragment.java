package com.yuji.cahayamas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yuji.cahayamas.adapter.RecordAdapter;
import com.yuji.cahayamas.models.Record;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecordFragment extends Fragment {
    private RecordAdapter adRecord;
    private RecyclerView recyclerView;
    private ArrayList<Record> records = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RecordFragment() {
    }

    public RecordFragment(ArrayList<Record> records) {
        this.records = records;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IncomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecordFragment newInstance(String param1, String param2) {
        RecordFragment fragment = new RecordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_record, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        recyclerView = getView().findViewById(R.id.recyclerView);
        adRecord = new RecordAdapter(getActivity(), records);
        recyclerView.setAdapter(adRecord);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public void addRecord(Record record) {
        records.add(0, record);
        adRecord.notifyItemInserted(0);
    }

    public void updateRecord(Record record, int position) {
        if (records.get(position) != null) {
            records.set(position, record);
            adRecord.notifyItemChanged(position);
        }
    }

    public void deleteRecord(int position) {
        if (records.get(position) != null) {
            records.remove(position);
            adRecord.notifyItemRemoved(position);
        }
    }

    public void setRecords(ArrayList<Record> records) {
        this.records = records;
    }
}