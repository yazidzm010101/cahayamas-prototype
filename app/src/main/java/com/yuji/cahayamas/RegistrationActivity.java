package com.yuji.cahayamas;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.yuji.cahayamas.models.DaoSession;
import com.yuji.cahayamas.models.User;
import com.yuji.cahayamas.models.UserDao;

public class RegistrationActivity extends AppCompatActivity {
    private UserDao userDao;
    private TextInputLayout txName;
    private TextInputLayout txUsername;
    private TextInputLayout txPassword;
    private TextInputLayout txRepassword;
    private Button btRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();

        setupViews();
    }

    protected void setupViews() {
        txName = findViewById(R.id.name);
        txUsername = findViewById(R.id.username);
        txPassword = findViewById(R.id.password);
        txRepassword = findViewById(R.id.repassword);
        btRegistration = findViewById(R.id.register);

        checkAllValid();

        btRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        txName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkAllValid();
            }
        });

        txUsername.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkUser();
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkAllValid();
            }
        });

        txPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkRepassword();
                checkAllValid();
            }
        });

        txRepassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkRepassword();
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkAllValid();
            }
        });

    }

    protected void checkUser() {
        String username = txUsername.getEditText().getText().toString();
        User user = userDao.queryBuilder().where(UserDao.Properties.Username.eq(username)).unique();
        if (user != null) {
            txUsername.setErrorEnabled(true);
            txUsername.setHelperTextEnabled(false);
            txUsername.setError("Username already exist");
            return;
        }
        txUsername.setErrorEnabled(false);
        txUsername.setHelperTextEnabled(true);
        txUsername.setHelperText("Username is available");
    }

    protected void checkRepassword() {
        String password = txPassword.getEditText().getText().toString();
        String repassword = txRepassword.getEditText().getText().toString();
        if (!password.equals(repassword)) {
            txRepassword.setErrorEnabled(true);
            txRepassword.setHelperTextEnabled(false);
            txRepassword.setError("Password didn't match");
            return;
        }
        txRepassword.setErrorEnabled(false);
        txRepassword.setHelperTextEnabled(true);
        txRepassword.setHelperText("Password matched");
    }

    protected void checkAllValid() {
        String name = txName.getEditText().getText().toString();
        String username = txUsername.getEditText().getText().toString();
        String password = txPassword.getEditText().getText().toString();
        String repassword = txRepassword.getEditText().getText().toString();
        boolean isValid = !name.isEmpty() && !username.isEmpty() && !password.isEmpty() && !repassword.isEmpty()
                && !txUsername.isErrorEnabled() && !txRepassword.isErrorEnabled();

        btRegistration.setEnabled(isValid);
    }

    protected void register() {
        String name = txName.getEditText().getText().toString();
        String username = txUsername.getEditText().getText().toString();
        String password = txPassword.getEditText().getText().toString();
        User user = new User(null, name, username, password);

        long id = userDao.insert(user);

        if (id < 1) {
            Toast.makeText(this, "Registration failed", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(this, "Registration success", Toast.LENGTH_LONG).show();
    }
}